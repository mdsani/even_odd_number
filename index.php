<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Even Odd</title>
    <!--bootstrap css-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        .main{
            width: 350px;
            margin: auto;
            box-shadow: 0px 10px 40px -10px gray;
            display: flex;
            justify-content: center;
            height: 270px;
            margin-top: 100px;
        }
        
        
        h3{
            text-align: center;
            margin-top: 10px;
            margin-bottom: 20px;
        }
        input{
            height: 45px;
            width: 100%;
            outline: none;
            font-size: 16px;
            border-radius: 5px;
            padding-left: 15px;
            border: 1px solid purple;
            border-bottom-width: 2px;
            transition: all 0.3s ease;
        }
        .btn{
            border: 1px solid purple;
            border-bottom-width: 2px;
            width: 48%;
        }
        .btn:hover{
            background-color: #DCDCDC;
        }
        .result{
            width: 350px;
            margin: auto;
            box-shadow: 0px 4px 12px -8px purple;
            display: flex;
            align-items: center;
            justify-content: start;
            padding: 10px;
            min-height: 50px;
        }
    </style>
</head>
<body>

<!--MAIN-->
<div class="main">
    <form action="" method = "post">
        <h3>Even & Odd</h3>
        <input type= "float" name="num1" placeholder="First number"><br><br>
        <input type="float" name="num2" placeholder="Second number"><br><br>
        <div>
            <button type="submit" class="btn" name="operat" value="even">Even</button>
            <button type="submit" class="btn" name="operat" value="odd">Odd</button>
        </div>
    </form>      
</div>



<!--POST-->
<div class="result">
<?php
include 'vendor/autoload.php';
use Sani\Math;

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $num1 = $_POST['num1'];
    $num2 = $_POST['num2'];
    $operat = $_POST['operat'];
    
    if(is_numeric($num1) && is_numeric($num2)){
        $result =new Math($num1, $num2);
        switch($operat){
            case 'even':
                echo $result->Even();
                break;
            case 'odd':
                echo $result->Odd();
                break;
        }
    }
}
?>
</div>



<!--bootstrap js-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>